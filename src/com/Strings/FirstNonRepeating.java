package com.string;

import java.util.ArrayList;
import java.util.List;

public class FirstNonRepeating {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(nonRepeat("bharath"));
		System.out.println(nonRepeat("aasd"));
		System.out.println(nonRepeat("raj"));

	}

	private static Character nonRepeat(String string) {
		char[] chars = string.toCharArray();

		List<Character> discardedChars = new ArrayList<>();

		for (int i = 0; i < chars.length; i++) {
			char c = chars[i];

			if (discardedChars.contains(c))
				continue;

			for (int j = i + 1; j < chars.length; j++) {
				if (c == chars[j]) { 
					discardedChars.add(c);
					break;
				} else if (j == chars.length - 1) { 
					return c;
				}
			}
		}
		return null;
	}


	}

