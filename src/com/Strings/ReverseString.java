package com.string;

import java.util.Scanner;

public class ReverseString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name: ");
		String nextLine = sc.nextLine();
		char[] charArray = nextLine.toCharArray();
		for (int i = charArray.length - 1; i >= 0; i--) {
			System.out.println(charArray[i]);

		}
		sc.close();
	}
}