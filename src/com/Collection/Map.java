package com.Collection;

import java.util.Collection;
import java.util.Set;
import java.util.TreeMap;

public class Map {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeMap<Character, Integer> m = new TreeMap<>();
		m.put('!', 10);
		m.put('@', 20);
		m.put('#', 30);
		m.put('$', 40);
		m.put('%', 50);
		m.put('&', 60);
		m.put('^', 70);
		m.put('*', 80);
		m.put('(', 20);
		m.put('(', 10);
		System.out.println(m);
		Set<Character> keySet = m.keySet();
		System.out.println(keySet);
		for (Character character : keySet) {
			System.out.println(character);
		}
		Collection<Integer> values = m.values();
		for (Integer integer : values) {
			System.out.println(integer);
		}
		Set<java.util.Map.Entry<Character, Integer>> keySet2 = m.entrySet();
		for (java.util.Map.Entry<Character, Integer> character1 : keySet2) {
			System.out.println(character1);
		}
		m.put('-', 10);
		System.out.println(m);
	}
}

