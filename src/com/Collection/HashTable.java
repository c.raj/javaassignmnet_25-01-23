package com.Collection;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class HashTable {

	public static void main(String[] args) {
		 Hashtable<Integer,String> map=new Hashtable<Integer,String>();
	     map.put(100,"Mango");
	     map.put(102,"Papaya");
	     map.put(101,"Kiwi");
	     map.put(103,"Lime");
	    
	     //Here, we specify the if and else statement as arguments of the method
	     System.out.println(map.getOrDefault(101, "Not Found"));
	     System.out.println(map.getOrDefault(105, "Not Found"));
	    
	    
	     System.out.println("Initial Map: "+map);
	    
	     for(Map.Entry m:map.entrySet()){
	    	   System.out.println(m.getKey()+" "+m.getValue());
	    	  }
	    
	    
	     //Inserts, as the specified pair is unique
	     map.putIfAbsent(104,"Banana");
	   
	     System.out.println("Updated Map: "+map);
	    
	     //Returns the current value, as the specified pair already exist
	     map.putIfAbsent(101,"Cherry");
	     System.out.println("Updated Map: "+map);
		
	}
}